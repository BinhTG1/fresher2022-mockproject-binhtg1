package com.example.mockproject.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mockproject.R
import com.example.mockproject.constant.APIConstant
import com.example.mockproject.model.CastCrew
import com.squareup.picasso.Picasso

class CastAdapter(private var castCrewList: ArrayList<CastCrew>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    fun updateList(newList: ArrayList<CastCrew>) {
        castCrewList = newList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.cc_item, parent, false)
        return CCViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currentItem = castCrewList[position]
        val url = APIConstant.BASE_IMG_URL + currentItem.profilePath
        if (holder is CCViewHolder) {
            holder.name.text = currentItem.name
            Picasso.get().load(url).error(R.drawable.ic_baseline_face_24).into(holder.pic)
        }

    }

    override fun getItemCount(): Int = castCrewList.size

    inner class CCViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.cast_name)
        var pic: ImageView = itemView.findViewById(R.id.cast_pic)
    }
}