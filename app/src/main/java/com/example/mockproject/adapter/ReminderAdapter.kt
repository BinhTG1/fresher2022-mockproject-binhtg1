package com.example.mockproject.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mockproject.R
import com.example.mockproject.constant.APIConstant
import com.example.mockproject.model.Movie
import com.squareup.picasso.Picasso

class ReminderAdapter(
    private var screenType: Int,
    private val listener: View.OnClickListener?,
    private val listenerLong: View.OnLongClickListener?,
    var movieList: ArrayList<Movie>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun updateList(newList: ArrayList<Movie>) {
        this.movieList = newList
        notifyDataSetChanged()
    }

    fun settingMovieFavorite(movieFavoriteList: ArrayList<Movie>) {
        var changed = false
        for (i in 0 until movieList.size) {
            if (movieList[i].isFavorite) {
                movieList[i].isFavorite = false
                changed = true
            }
            for (j in 0 until movieFavoriteList.size) {
                if (movieList[i].id == movieFavoriteList[j].id) {
                    movieList[i].isFavorite = true
                    changed = !changed
                    break
                }
            }
        }
    }

    fun deleteItem(pos: Int) {
        movieList.removeAt(pos)
        notifyDataSetChanged()
    }

    fun updateItem(movie: Movie) {
        for (i in 0 until movieList.size) {
            if (movie.id == movieList[i].id) {
                this.movieList[i] = movie
                notifyItemChanged(i)
                return
            }
        }
        if (movieList.size < 3) {
            this.movieList.add(movie)
            notifyItemInserted(movieList.size)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (screenType == 1) {
            val itemView =
                LayoutInflater.from(parent.context).inflate(R.layout.reminder_drawer, parent, false)
            return ReminderDrawerHolder(itemView)
        }
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.reminder_item, parent, false)
        return ReminderHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currentItem = movieList[position]
        val url = APIConstant.BASE_IMG_URL + currentItem.posterPath
        when (holder) {
            is ReminderAdapter.ReminderHolder -> {
                holder.itemView.tag = position
                holder.itemView.setOnClickListener(listener)
                holder.itemView.setOnLongClickListener(listenerLong)
                holder.title.text =
                    currentItem.title.plus(" - ${currentItem.releaseDate.take(4)} - ${currentItem.voteAverage}/10")
                Picasso.get().load(url).into(holder.poster)
                holder.subtitle.text = currentItem.dateHourReminder
            }
            is ReminderAdapter.ReminderDrawerHolder -> {
                holder.title.text =
                    currentItem.title.plus(" - ${currentItem.releaseDate.take(4)} - ${currentItem.voteAverage}/10")
                holder.subtitle.text = currentItem.dateHourReminder
            }
        }

    }

    override fun getItemCount(): Int = movieList.size

    inner class ReminderDrawerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.reminder_title_drawer)
        var subtitle: TextView = itemView.findViewById(R.id.reminder_subtitle_drawer)
    }

    inner class ReminderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var poster: ImageView = itemView.findViewById(R.id.reminder_poster_all)
        var title: TextView = itemView.findViewById(R.id.reminder_title_all)
        var subtitle: TextView = itemView.findViewById(R.id.reminder_subtitle_all)
    }

}