package com.example.mockproject.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mockproject.R
import com.example.mockproject.constant.APIConstant
import com.example.mockproject.model.Movie
import com.squareup.picasso.Picasso

class MovieAdapter(
    var movieList: ArrayList<Movie>,
    private val listener: View.OnClickListener,
    private var screenType: Int,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun updateList(newList: ArrayList<Movie>) {
        this.movieList = newList
        notifyDataSetChanged()
    }

    fun updateItem(pos: Int) {
        this.movieList[pos].isFavorite = true
        notifyItemChanged(pos)
    }

    fun settingMovieFavorite(movieFavoriteList: ArrayList<Movie>) {
        var changed = false
        for (i in 0 until movieList.size) {
            if (movieList[i].isFavorite) {
                movieList[i].isFavorite = false
                changed = true
            }
            for (j in 0 until movieFavoriteList.size) {
                if (movieList[i].id == movieFavoriteList[j].id) {
                    movieList[i].isFavorite = true
                    changed = !changed
                    break
                }
            }
            if (changed) {
                notifyItemChanged(i)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (screenType == 1) {
            val itemView =
                LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
            return MovieViewHolder(itemView)
        }
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.movie_item_grid, parent, false)
        return GridViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currentItem = movieList[position]
        val url = APIConstant.BASE_IMG_URL + currentItem.posterPath
        holder.itemView.tag = position
        holder.itemView.setOnClickListener(listener)
        when (holder) {
            is MovieViewHolder -> {
                holder.title.text = currentItem.title
                Picasso.get().load(url).into(holder.img)
                if (currentItem.adult) {
                    holder.adult.visibility = View.VISIBLE
                }
                holder.releaseDate.text = "Release date: ".plus(currentItem.releaseDate)
                holder.rating.text = "Rating: ".plus(currentItem.voteAverage).plus("/10.0")
                holder.overview.text = currentItem.overview
                if (currentItem.isFavorite) {
                    holder.favButton.setImageResource(R.drawable.ic_action_star)
                } else {
                    holder.favButton.setImageResource(R.drawable.ic_action_star_border)
                }
                holder.favButton.tag = position
                holder.favButton.setOnClickListener(listener)
            }
            is GridViewHolder -> {
                holder.gridTitle.text = currentItem.title
                Picasso.get().load(url).into(holder.gridImg)
            }
        }
    }

    override fun getItemCount(): Int = movieList.size

    inner class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.movie_title)
        var img: ImageView = itemView.findViewById(R.id.img)
        var releaseDate: TextView = itemView.findViewById(R.id.release_date)
        var rating: TextView = itemView.findViewById(R.id.rating)
        var overview: TextView = itemView.findViewById(R.id.overview_text)
        var favButton: ImageButton = itemView.findViewById(R.id.favorite_button)
        var adult: ImageView = itemView.findViewById(R.id.adult)


    }

    inner class GridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var gridTitle: TextView = itemView.findViewById(R.id.grid_title)
        var gridImg: ImageView = itemView.findViewById(R.id.grid_img)
    }

}