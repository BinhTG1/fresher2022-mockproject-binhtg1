package com.example.mockproject

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.mockproject.adapter.ReminderAdapter
import com.example.mockproject.adapter.ViewPagerAdapter
import com.example.mockproject.callback.*
import com.example.mockproject.database.DatabaseOpenHelper
import com.example.mockproject.model.Movie
import com.example.mockproject.util.ImageConverter
import com.example.mockproject.view.*
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import de.hdodenhof.circleimageview.CircleImageView

class MainActivity : AppCompatActivity(), MovieCallBack, ProfileCallBack, FavoriteCallBack,
    ReminderCallBack, DetailCallBack, SettingCallBack {
    private lateinit var mViewPager: ViewPager2
    private lateinit var mTabLayout: TabLayout
    private val mDatabaseOpenHelper: DatabaseOpenHelper =
        DatabaseOpenHelper(this, "Database", null, 1)
    private lateinit var movieFragment: MovieFragment
    private lateinit var favoriteFragment: FavoriteFragment
    private lateinit var settingsFragment: SettingsFragment
    private lateinit var aboutFragment: AboutFragment
    private lateinit var reminderFragment: ReminderFragment
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var drawer: DrawerLayout
    private lateinit var navView: NavigationView
    private lateinit var toolbar: Toolbar
    private lateinit var headerView: View
    private lateinit var reminderRecyclerView: RecyclerView
    private lateinit var reminderAdapter: ReminderAdapter
    private lateinit var profileFragment: ProfileFragment
    private lateinit var dob: TextView
    private lateinit var name: TextView
    private lateinit var mail: TextView
    private lateinit var gender: TextView
    private lateinit var bitmap: Bitmap
    private lateinit var badgeDrawable: BadgeDrawable
    private lateinit var tv6:TextView
    private lateinit var showAll:Button

    private var badge = 0

    private lateinit var profilePic: CircleImageView

    private lateinit var pref: SharedPreferences

    private var imgConverter: ImageConverter = ImageConverter()

    var drawerReminderList = ArrayList<Movie>()


    private var mIconList = listOf(
        R.drawable.ic_action_home,
        R.drawable.ic_action_favorite,
        R.drawable.ic_action_settings,
        R.drawable.ic_action_info
    )
    private var mTitleList = listOf("Movie", "Favorite", "Settings", "About")

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pref = androidx.preference.PreferenceManager.getDefaultSharedPreferences(this)
        badge = mDatabaseOpenHelper.getListMovie().size
        setUpData()
        setUpDrawer()
        setUpTabs()
        movieFragment.setMovieCallBack(this)
        favoriteFragment.setFavoriteCallBack(this)
        settingsFragment.setsettingCallBack(this)
        createNotificationChannel()
    }


    fun setUpData() {
        drawerReminderList = mDatabaseOpenHelper.getListReminderForDrawer()
        reminderAdapter = ReminderAdapter(1, null, null, drawerReminderList)

    }

    private fun setUpDrawer() {
        toolbar = findViewById(R.id.toolbar_main)
        setSupportActionBar(toolbar)
        drawer = findViewById(R.id.draw)
        navView = findViewById(R.id.nav_view)
        toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.open, R.string.close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        setUpDrawerContent()
    }

    private fun setUpDrawerContent() {
        headerView = navView.getHeaderView(0)


        reminderRecyclerView = headerView.findViewById(R.id.reminder_recycler_view)
        reminderRecyclerView.adapter = reminderAdapter
        reminderRecyclerView.layoutManager = LinearLayoutManager(this)
        profilePic = headerView.findViewById(R.id.pro_drawer_pic)
        mail = headerView.findViewById<TextView>(R.id.mail)
        gender = headerView.findViewById<TextView>(R.id.gender)
        dob = headerView.findViewById(R.id.dob)
        name = headerView.findViewById<TextView>(R.id.pro_drawer_name)
        try {
            profilePic.setImageBitmap(
                imgConverter.StringToBitMap(
                    pref.getString(
                        "profileImg",
                        "TextView"
                    )
                )
            )
        } catch (e: Exception) {
            profilePic.setImageResource(R.drawable.ic_baseline_face_24)
        }
        name.text = pref.getString("profileName", "TextView")
        mail.text = pref.getString("profileMail", "TextView")
        dob.text = pref.getString("profileDob", "1990-1-1")
        gender.text = pref.getString("profileGender", "TextView")


        val editButton: Button = headerView.findViewById(R.id.button_edit)
        editButton.setOnClickListener {
            if (this.drawer.isDrawerOpen(GravityCompat.START)) {
                this.drawer.closeDrawer(GravityCompat.START)
            }
            profileFragment = ProfileFragment()
            profileFragment.setProfilCallBack(this)
            val bundle = Bundle()
            bundle.putString("name", name.text.toString())
            bundle.putString("email", mail.text.toString())
            bundle.putString("dob", dob.text.toString())
            bundle.putString("gender", gender.text.toString())
            try {
                bundle.putString("imgBitMapString", imgConverter.BitMapToString(bitmap))
            } catch (e: Exception) {

            }
            profileFragment.arguments = bundle
            if (!profileFragment.isAdded) {
                supportFragmentManager.beginTransaction().apply {
                    add(R.id.relative, profileFragment)
                    addToBackStack(null)
                    commit()
                }
            }

        }
        showAll = headerView.findViewById(R.id.button_show_all)
        showAll.setOnClickListener {
            if (this.drawer.isDrawerOpen(GravityCompat.START)) {
                this.drawer.closeDrawer(GravityCompat.START)
            }
            reminderFragment = ReminderFragment(mDatabaseOpenHelper)
            reminderFragment.setReminderCallBack(this)
            supportFragmentManager.beginTransaction().apply {
                add(R.id.draw, reminderFragment, "REMINDER_FRAGMENT")
                addToBackStack(null)
                commit()
            }
        }

        if(drawerReminderList.isEmpty()) {
            showAll.visibility = View.GONE
            reminderRecyclerView.visibility = View.GONE
            tv6 = headerView.findViewById(R.id.textView6)
            tv6.visibility = View.GONE
        }
    }

    private fun setUpTabs() {
        mViewPager = findViewById(R.id.viewPager2)
        mTabLayout = findViewById(R.id.tabLayout2)
        movieFragment = MovieFragment(mDatabaseOpenHelper)
        favoriteFragment = FavoriteFragment(mDatabaseOpenHelper)
        settingsFragment = SettingsFragment()
        aboutFragment = AboutFragment()


        val mViewPagerAdapter = ViewPagerAdapter(this)
        mViewPagerAdapter.addFragment(movieFragment)
        mViewPagerAdapter.addFragment(favoriteFragment)
        mViewPagerAdapter.addFragment(settingsFragment)
        mViewPagerAdapter.addFragment(aboutFragment)
        mViewPager.adapter = mViewPagerAdapter
        mViewPager.offscreenPageLimit = 4

        TabLayoutMediator(
            mTabLayout,
            mViewPager
        ) { tab, position ->
            tab.setIcon(mIconList[position])
            tab.text = mTitleList[position]
            if (position == 1) {
                badgeDrawable = tab.orCreateBadge
                badgeDrawable.isVisible = false
                badgeDrawable.backgroundColor = ContextCompat.getColor(
                    applicationContext,
                    androidx.appcompat.R.color.accent_material_light
                )
                if (badge > 0) {
                    badgeDrawable.isVisible = true
                    badgeDrawable.number = badge
                    badgeDrawable.maxCharacterCount = 2
                }
            }
        }.attach()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.movie_menu -> {
                mViewPager.setCurrentItem(0, true)
            }
            R.id.favorites_menu -> {
                mViewPager.setCurrentItem(1, true)
            }
            R.id.settings_menu -> {
                mViewPager.setCurrentItem(2, true)
            }
            R.id.about_menu -> {
                mViewPager.setCurrentItem(3, true)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val channel =
            NotificationChannel("channel1", "Movie", NotificationManager.IMPORTANCE_DEFAULT)
        channel.description = "It's morbin time"
        val notificationManager =
            getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }


    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onSaveProfile(
        name: String,
        mail: String,
        dob: String,
        gender: String,
        bitmapProfile: Bitmap?
    ) {
        val edit = pref.edit()
        edit.putString("profileName", name)
        edit.putString("profileMail", mail)
        edit.putString("profileDob", dob)
        edit.putString("profileGender", gender)
        if (bitmapProfile != null) {
            edit.putString("profileImg", imgConverter.BitMapToString(bitmapProfile))
        }
        edit.apply()
        loadProfileData()
    }

    fun loadProfileData() {
        name.text = pref.getString("profileName", "TextView")
        mail.text = pref.getString("profileMail", "TextView")
        dob.text = pref.getString("profileDob", "TextView")
        gender.text = pref.getString("profileGender", "TextView")

        try {
            profilePic.setImageBitmap(
                imgConverter.StringToBitMap(
                    pref.getString(
                        "profileImg",
                        "TextView"
                    )
                )
            )
        } catch (e: Exception) {
            profilePic.setImageResource(R.drawable.ic_baseline_face_24)
        }

    }


    override fun updateBadge(isAdded: Boolean) {
        if (isAdded) {
            badge++
        } else {
            badge--
        }
        if (badge == 0) {
            badgeDrawable.isVisible = false
        } else {
            badgeDrawable.isVisible = true
            badgeDrawable.number = badge
        }

    }

    override fun updateFavo(id: Int, isFavo: Boolean) {
        movieFragment.updateFavo(id, isFavo)
    }

    override fun onLoadReminder() {
        reminderAdapter.updateList(mDatabaseOpenHelper.getListReminderForDrawer())
        if(reminderAdapter.movieList.isNotEmpty()) {
            showAll.visibility = View.VISIBLE
            reminderRecyclerView.visibility = View.VISIBLE
            tv6.visibility = View.VISIBLE
        } else {
            showAll.visibility = View.GONE
            reminderRecyclerView.visibility = View.GONE
            tv6.visibility = View.GONE
        }
    }

    override fun onClickItemReminder(movie: Movie) {
        mViewPager.currentItem = 0
        if (this.drawer.isDrawerOpen(GravityCompat.START)) {
            this.drawer.closeDrawer(GravityCompat.START)
        }

        val reminderCurrentFragment = supportFragmentManager.findFragmentByTag("REMINDER_FRAGMENT")
        if (reminderCurrentFragment != null) {
            supportFragmentManager.beginTransaction().apply {
                remove(reminderCurrentFragment)
                commit()
            }
        }
        val detailCurrentFragment =
            supportFragmentManager.findFragmentByTag("DETAIL_FRAGMENT")
        if (detailCurrentFragment != null) {
            supportFragmentManager.beginTransaction().apply {
                remove(detailCurrentFragment)
                commit()
            }
        }
        val detailFragment = DetailFragment(mDatabaseOpenHelper)
        val bundle = Bundle()
        bundle.putSerializable("movieDetail", movie)

        detailFragment.setdetailCallBack(this)
        detailFragment.arguments = bundle
        supportFragmentManager.beginTransaction().apply {
            add(R.id.how, detailFragment, "FRAGMENT_DETAIL")
            addToBackStack(null)
            commit()
        }
    }

    override fun onUpdateFromSetting(
        category: String,
        rate: String,
        releaseYear: String,
        sort: String
    ) {
        movieFragment.setListMovieByCondition(category, rate.toDouble(), releaseYear, sort)
    }
}