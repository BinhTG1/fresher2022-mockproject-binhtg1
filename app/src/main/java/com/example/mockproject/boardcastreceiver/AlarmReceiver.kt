package com.example.mockproject.boardcastreceiver

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.mockproject.R
import com.example.mockproject.database.DatabaseOpenHelper

class AlarmReceiver : BroadcastReceiver() {
    private val channelID = "channel1"
    private val notificationID = 1
    override fun onReceive(context: Context, intent: Intent) {
        val notification = NotificationCompat.Builder(context, channelID)
            .setSmallIcon(R.drawable.ic_baseline_movie_24)
            .setContentTitle(intent.getStringExtra("titleExtra"))
            .setContentText(intent.getStringExtra("messageExtra"))
            .build()
        val bundle = intent.extras
        if (bundle != null) {
            val id = bundle.getInt("movieId")
            val time = bundle.getString("time")
            val databaseOpenHelper = DatabaseOpenHelper(context, "Database", null, 1)
            if (databaseOpenHelper.deleteReminder(id) > 0) {
                Log.e("TAG", "Removed")
            } else {
                Log.e("TAG", "Remove failed")
            }
        }
        val manager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(notificationID, notification)
    }

}