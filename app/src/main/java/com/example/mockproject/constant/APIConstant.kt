package com.example.mockproject.constant

class APIConstant {
    companion object {
        val BASE_URL = "https://api.themoviedb.org/"
        val BASE_IMG_URL = "https://image.tmdb.org/t/p/original"
        var API_KEY = "e7631ffcb8e766993e5ec0c1f4245f93"
        val PREFS_NAME = "prefName"
    }
}