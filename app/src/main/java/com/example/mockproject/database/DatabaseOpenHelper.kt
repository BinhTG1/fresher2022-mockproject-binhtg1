package com.example.mockproject.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.example.mockproject.model.Movie

class DatabaseOpenHelper(
    context: Context?,
    name: String?,
    factory: SQLiteDatabase.CursorFactory?,
    version: Int
) : SQLiteOpenHelper(context, name, factory, version) {
    companion object {
        private var MOVIE_TABLE = "movie_table"
        private var MOVIE_ID = "movie_id"
        private var MOVIE_TITLE = "movie_name"
        private var MOVIE_RATING = "movie_rating"
        private var MOVIE_OVERVIEW = "movie_overview"
        private var MOVIE_DATE = "movie_date"
        private var MOVIE_IMAGE_POSTER = "movie_image"
        private var MOVIE_ADULT = "movie_adult"
        private var MOVIE_FAVORITE = "movie_favorite"
        private var REMINDER_TIME = "movie_reminder"
        private var REMINDER_TABLE = "reminder_table"
        private var REMINDER_DATE_HOUR = "reminder_date_hour"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTableMovie = "CREATE TABLE $MOVIE_TABLE ( " +
                "$MOVIE_ID INTEGER PRIMARY KEY," +
                "$MOVIE_TITLE TEXT," +
                "$MOVIE_OVERVIEW TEXT," +
                "$MOVIE_RATING REAL," +
                "$MOVIE_DATE TEXT," +
                "$MOVIE_IMAGE_POSTER TEXT," +
                "$MOVIE_ADULT INTEGER," +
                "$MOVIE_FAVORITE INTEGER)"

        val createTableReminder = "CREATE TABLE $REMINDER_TABLE ( " +
                "$MOVIE_ID INTEGER PRIMARY KEY," +
                "$MOVIE_TITLE TEXT," +
                "$MOVIE_OVERVIEW TEXT," +
                "$MOVIE_RATING REAL," +
                "$MOVIE_DATE TEXT," +
                "$MOVIE_IMAGE_POSTER TEXT," +
                "$MOVIE_ADULT INTEGER," +
                "$MOVIE_FAVORITE INTEGER," +
                "$REMINDER_TIME TEXT," +
                "$REMINDER_DATE_HOUR TEXT)"

        db?.execSQL(createTableMovie)
        db?.execSQL(createTableReminder)

    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        val dropTableMovie = "DROP TABLE IF EXIST $MOVIE_TABLE"
        val dropTableReminder = "DROP TABLE IF EXIST $REMINDER_TABLE"
        p0!!.execSQL(dropTableMovie)
        p0.execSQL(dropTableReminder)
        onCreate(p0)
    }

    fun getListMovie(): ArrayList<Movie> {
        val listMovie: ArrayList<Movie> = ArrayList()
        val selectQuery = "SELECT * FROM $MOVIE_TABLE"
        val db = this.readableDatabase
        val cursor: Cursor
        var movie: Movie
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        if (cursor.moveToFirst()) {
            do {
                movie = Movie(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getDouble(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getInt(6) == 0,
                    cursor.getInt(7) == 0
                )
                listMovie.add(movie)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return listMovie
    }

    fun getListReminder(): ArrayList<Movie> {
        val listMovie: ArrayList<Movie> = ArrayList()
        val selectQuery = "SELECT * FROM $REMINDER_TABLE"
        val db = this.readableDatabase
        val cursor: Cursor
        var movie: Movie
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        if (cursor.moveToFirst()) {
            do {
                movie = Movie(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getDouble(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getInt(6) == 0,
                    cursor.getInt(7) == 0,
                    cursor.getString(8),
                    cursor.getString(9)
                )
                listMovie.add(movie)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return listMovie
    }

    fun getListReminderForDrawer(): ArrayList<Movie> {
        val listMovie: ArrayList<Movie> = ArrayList()
        val selectQuery = "SELECT * FROM $REMINDER_TABLE LIMIT 3"
        val db = this.readableDatabase
        val cursor: Cursor
        var movie: Movie
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        if (cursor.moveToFirst()) {
            do {
                movie = Movie(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getDouble(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getInt(6) == 0,
                    cursor.getInt(7) == 0,
                    cursor.getString(8),
                    cursor.getString(9)
                )
                listMovie.add(movie)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return listMovie
    }

    fun checkReminderExist(movieId: Int): Int {
        val listMovie: ArrayList<Movie> = ArrayList()
        val selectQuery =
            "SELECT * FROM $REMINDER_TABLE WHERE $MOVIE_ID = $movieId"
        val db = this.readableDatabase
        val cursor: Cursor
        var movie: Movie
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return 0
        }
        if (cursor.moveToFirst()) {
            do {
                movie = Movie(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getDouble(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getInt(6) == 0,
                    cursor.getInt(7) == 0,
                    cursor.getString(8),
                    cursor.getString(9)
                )
                listMovie.add(movie)
                if (listMovie.size > 0) {
                    return 1
                }
            } while (cursor.moveToNext())
        }
        cursor.close()
        return 0
    }

    fun addMovie(movie: Movie): Int {
        val db = this.writableDatabase
        val contestValues = ContentValues()
        contestValues.put(MOVIE_ID, movie.id)
        contestValues.put(MOVIE_TITLE, movie.title)
        contestValues.put(MOVIE_OVERVIEW, movie.overview)
        contestValues.put(MOVIE_RATING, movie.voteAverage)
        contestValues.put(MOVIE_DATE, movie.releaseDate)
        contestValues.put(MOVIE_IMAGE_POSTER, movie.posterPath)
        if (movie.adult) {
            contestValues.put(MOVIE_ADULT, 0)
        } else {
            contestValues.put(MOVIE_ADULT, 1)
        }
        contestValues.put(MOVIE_FAVORITE, 0)
        val success = db.insert(MOVIE_TABLE, null, contestValues)
        db.close()
        return success.toInt()
    }

    fun addReminder(movie: Movie): Int {
        val db = this.writableDatabase
        val contestValues = ContentValues()
        contestValues.put(MOVIE_ID, movie.id)
        contestValues.put(MOVIE_TITLE, movie.title)
        contestValues.put(MOVIE_OVERVIEW, movie.overview)
        contestValues.put(MOVIE_RATING, movie.voteAverage)
        contestValues.put(MOVIE_DATE, movie.releaseDate)
        contestValues.put(MOVIE_IMAGE_POSTER, movie.posterPath)
        if (movie.adult) {
            contestValues.put(MOVIE_ADULT, 0)
        } else {
            contestValues.put(MOVIE_ADULT, 1)
        }
        if (movie.isFavorite) {
            contestValues.put(MOVIE_FAVORITE, 0)
        } else {
            contestValues.put(MOVIE_FAVORITE, 1)
        }
        contestValues.put(REMINDER_TIME, movie.reminderTime)
        contestValues.put(REMINDER_DATE_HOUR, movie.dateHourReminder)
        val success = db.insert(REMINDER_TABLE, null, contestValues)
        db.close()
        return success.toInt()
    }

    fun deleteMovie(id: Int): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(MOVIE_ID, id)
        val success = db.delete(MOVIE_TABLE, "$MOVIE_ID = $id", null)
        db.close()
        return success
    }

    fun deleteReminder(id: Int): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(MOVIE_ID, id)
        val success =
            db.delete(REMINDER_TABLE, "$MOVIE_ID = $id ", null)
        db.close()
        return success
    }

    fun updateReminder(movie: Movie): Int {
        val db = this.writableDatabase
        val contestValues = ContentValues()
        contestValues.put(REMINDER_TIME, movie.reminderTime)
        contestValues.put(REMINDER_DATE_HOUR, movie.dateHourReminder)
        val success = db.update(
            REMINDER_TABLE,
            contestValues,
            "movie_id = ?",
            Array(1) { movie.id.toString() })
        db.close()
        return success
    }
}