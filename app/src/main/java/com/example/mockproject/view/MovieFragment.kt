package com.example.mockproject.view

import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.mockproject.R
import com.example.mockproject.adapter.MovieAdapter
import com.example.mockproject.callback.ApiCallBack
import com.example.mockproject.callback.DetailCallBack
import com.example.mockproject.callback.MovieCallBack
import com.example.mockproject.controller.ApiData
import com.example.mockproject.database.DatabaseOpenHelper
import com.example.mockproject.model.Movie
import java.lang.Exception

class MovieFragment(
    private val databaseOpenHelper: DatabaseOpenHelper,
    ) : Fragment(),
    View.OnClickListener, DetailCallBack, ApiCallBack {
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    lateinit var adapter: MovieAdapter
    private lateinit var movieCallBack: MovieCallBack
    private var movies = ArrayList<Movie>()
    private val apiData: ApiData = ApiData()
    private var page = 1
    private var screenType = 1
    private lateinit var movieDetailFragment: DetailFragment
    private lateinit var progressBar: ProgressBar
    private lateinit var handler: Handler
    private lateinit var category:String
    private var rate:Double = 0.0
    private lateinit var date:String
    private lateinit var sort:String
    private lateinit var pref: SharedPreferences


    fun setMovieCallBack(CallBack: MovieCallBack) {
        movieCallBack = CallBack
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_movie, container, false)
        requireActivity().title = "Movies"
        adapter = MovieAdapter(movies, this, screenType)
        progressBar = v.findViewById(R.id.progress)
        pref = androidx.preference.PreferenceManager.getDefaultSharedPreferences(requireContext())
        category = pref.getString("movie_category", "popular").toString()
        rate = pref.getInt("movie_rated", 0).toDouble()
        date = pref.getString("movie_release_year", "None").toString()
        sort = pref.getString("movie_sort", "none").toString()

        apiData.setapiCallBack(this)
        movieDetailFragment = DetailFragment(databaseOpenHelper)
        movieDetailFragment.setdetailCallBack(this)


        recyclerView = v.findViewById(R.id.movies)
        recyclerView.adapter = adapter
        if (screenType == 1) {
            recyclerView.layoutManager = LinearLayoutManager(context)
        } else {
            recyclerView.layoutManager = GridLayoutManager(context, 2)
        }
        recyclerView.setHasFixedSize(true)

        swipeRefreshLayout = v.findViewById(R.id.refresh)
        handler = Handler(Looper.getMainLooper())
        apiData.getListMovieFromApi(
            page,
            movies,
            adapter,
            databaseOpenHelper.getListMovie(),
            category,
            rate,
            date,
            sort,
            false,
            false
        )
        setHasOptionsMenu(true)

        swipeRefreshLayout.setOnRefreshListener {
            page = 1
            movies.clear()
            apiData.getListMovieFromApi(
                page,
                movies,
                adapter,
                databaseOpenHelper.getListMovie(),
                category,
                rate,
                date,
                sort,
                false,
                true
            )
            handler.postDelayed({
                swipeRefreshLayout.isRefreshing = false
            }, 1500)
        }
        loadMore()
        return v
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.favorite_button -> {
                val pos = view.tag as Int
                val movieItem = movies[pos]
                if (movieItem.isFavorite) {
                    if (databaseOpenHelper.deleteMovie(movieItem.id) > -1) {
                        movieItem.isFavorite = false
                        adapter.notifyItemChanged(pos)
                        movieCallBack.updateBadge(false)

                    } else {
                        Log.e("eeeeeeeeeeeeeeeeeee", "Remove failed")
                    }
                } else {
                    if (databaseOpenHelper.addMovie(movieItem) > -1) {
                        movieItem.isFavorite = true
                        adapter.notifyItemChanged(pos)
                        movieCallBack.updateBadge(true)
                    } else {
                        Log.e("eeeeeeeeeeeeeeeeeee", "add failed")
                    }
                }
            }
            R.id.movie_item -> {
                val pos = view.tag as Int
                val movieItem = movies[pos]
                val bundle = Bundle()
                bundle.putSerializable("movieDetail", movieItem)
                movieDetailFragment.arguments = bundle
                val trans = requireActivity().supportFragmentManager.beginTransaction()
                trans.add(R.id.how, movieDetailFragment, "DETAIL_FRAGMENT")
                trans.addToBackStack(null)
                trans.commit()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        requireActivity().title = "Movies"
        adapter.settingMovieFavorite(databaseOpenHelper.getListMovie())
        if (movieDetailFragment.arguments != null) {
            movieDetailFragment.check()
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.getItem(4)
        item.isVisible = true
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.change_view) {
            if (screenType == 0) {
                screenType = 1
                item.setIcon(R.drawable.ic_baseline_view_module_24)
                changeLayout()
                recyclerView.layoutManager = LinearLayoutManager(context)
            } else {
                screenType = 0
                item.setIcon(R.drawable.ic_baseline_view_agenda_24)
                changeLayout()
                recyclerView.layoutManager = GridLayoutManager(context, 2)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun changeLayout() {
        adapter = MovieAdapter(movies, this, screenType)
        recyclerView.adapter = adapter
    }


    override fun updateBadge(isAdded: Boolean) {
        movieCallBack.updateBadge(isAdded)
    }

    override fun updateFavo(id: Int, isFavo: Boolean) {
        for (i in 0 until movies.size) {
            if (id == movies[i].id) {
                movies[i].isFavorite = isFavo
                adapter.movieList[i].isFavorite = isFavo
                adapter.notifyItemChanged(i)
            }
        }
    }

    override fun onLoadReminder() {
        movieCallBack.onLoadReminder()
    }

    fun loadMore() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isLastItemDiplaying(recyclerView)) {
                    progressBar.visibility = View.VISIBLE
                    handler.postDelayed({
                        page = apiData.getListMovieFromApi(
                            page,
                            movies,
                            adapter,
                            databaseOpenHelper.getListMovie(),
                            category,
                            rate,
                            date,
                            sort,
                            true,
                            false
                        )
                        progressBar.visibility = View.GONE
                    }, 1000)
                }
            }
        })
    }

    fun isLastItemDiplaying(recyclerView: RecyclerView): Boolean {
        if (recyclerView.adapter!!.itemCount != 0) {
            val lastVisible =
                (recyclerView.layoutManager as LinearLayoutManager?)!!.findLastVisibleItemPosition()
            if (lastVisible != RecyclerView.NO_POSITION && lastVisible == recyclerView.adapter!!.itemCount - 1) {
                return true
            }
        }
        return false
    }

    fun setListMovieByCondition(cat: String, rat: Double, ate: String, sor: String) {
        category = cat
        rate = rat
        date = ate
        sort = sor
        page = 1
        apiData.getListMovieFromApi(
            page,
            movies,
            adapter,
            databaseOpenHelper.getListMovie(),
            category,
            rate,
            date,
            sort,
            false,
            true
        )
    }

    override fun loadProgress(isDone: Boolean) {
        if(isDone) {
            progressBar.visibility = View.GONE
        } else {
            progressBar.visibility = View.VISIBLE
        }
    }

}