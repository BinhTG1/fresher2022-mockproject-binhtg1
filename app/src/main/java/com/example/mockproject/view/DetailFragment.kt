package com.example.mockproject.view

import android.app.AlarmManager
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mockproject.R
import com.example.mockproject.adapter.CastAdapter
import com.example.mockproject.boardcastreceiver.AlarmReceiver
import com.example.mockproject.callback.DetailCallBack
import com.example.mockproject.constant.APIConstant
import com.example.mockproject.controller.ApiData
import com.example.mockproject.database.DatabaseOpenHelper
import com.example.mockproject.model.CastCrew
import com.example.mockproject.model.Movie
import com.example.mockproject.util.NotificationUtil
import com.squareup.picasso.Picasso
import java.util.*

class DetailFragment(private val databaseOpenHelper: DatabaseOpenHelper) : Fragment() {

    lateinit var favoriteButton: ImageButton
    lateinit var releaseDate: TextView
    lateinit var rating: TextView
    lateinit var overview: TextView
    lateinit var recyclerView: RecyclerView
    lateinit var poster: ImageView
    lateinit var movie: Movie
    lateinit var castAdapter: CastAdapter
    lateinit var button: Button
    lateinit var detailCallBack: DetailCallBack

    private val apiData: ApiData = ApiData()
    var castCrewList = ArrayList<CastCrew>()
    var saveYear: Int = 0
    var saveMonth: Int = 0
    var saveDay: Int = 0
    var saveHour: Int = 0
    var saveMinute: Int = 0
    val notificationID = 1


    fun setdetailCallBack(callBack: DetailCallBack) {
        detailCallBack = callBack
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_detail, container, false)
        val bundle = this.arguments
        if (bundle != null) {
            movie = bundle.getSerializable("movieDetail") as Movie
        }
        requireActivity().title = movie.title
        favoriteButton = v.findViewById(R.id.fav_button)
        releaseDate = v.findViewById(R.id.release_detail)
        rating = v.findViewById(R.id.rating_detail)
        overview = v.findViewById(R.id.overview_detail)
        recyclerView = v.findViewById(R.id.cast_and_crew)
        poster = v.findViewById(R.id.poster_detail)
        button = v.findViewById(R.id.reminder_button)

        if (movie.isFavorite) {
            favoriteButton.setImageResource(R.drawable.ic_action_star)
        } else {
            favoriteButton.setImageResource(R.drawable.ic_action_star_border)
        }

        castAdapter = CastAdapter(castCrewList)
        recyclerView.adapter = castAdapter
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)

        val url = APIConstant.BASE_IMG_URL + movie.posterPath
        Picasso.get().load(url).into(poster)
        releaseDate.text = "Release date: ".plus(movie.releaseDate)
        rating.text = "Rating: ".plus(movie.voteAverage)
        overview.text = movie.overview

        apiData.getCastCrewFromApi(castCrewList, castAdapter, movie.id)


        favoriteButton.setOnClickListener {
            if (movie.isFavorite) {
                if (databaseOpenHelper.deleteMovie(movie.id) > -1) {
                    movie.isFavorite = false
                    favoriteButton.setImageResource(R.drawable.ic_action_star_border)
                    detailCallBack.updateBadge(false)
                } else {
                    Log.e("eeeeeeeeeeeeeeeeeee", "Remove failed")
                }
            } else {
                if (databaseOpenHelper.addMovie(movie) > -1) {
                    movie.isFavorite = true
                    favoriteButton.setImageResource(R.drawable.ic_action_star)
                    detailCallBack.updateBadge(true)
                } else {
                    Log.e("eeeeeeeeeeeeeeeeeee", "add failed")
                }
            }
        }

        button.setOnClickListener {
            createReminder()

        }

        return v
    }

    fun check() {
        if (movie.isFavorite) {
            favoriteButton.setImageResource(R.drawable.ic_action_star)
        } else {
            favoriteButton.setImageResource(R.drawable.ic_action_star_border)
        }
    }

    override fun onDestroy() {
        requireActivity().title = "Movies"
        detailCallBack.updateFavo(movie.id, movie.isFavorite)
        super.onDestroy()
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun createReminder() {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        val startHour = currentDateTime.get(Calendar.HOUR_OF_DAY)
        val startMinute = currentDateTime.get(Calendar.MINUTE)


        DatePickerDialog(requireContext(), { _, year, month, day ->
            TimePickerDialog(requireContext(), { _, hour, minute ->
                val pickedDateTime = Calendar.getInstance()
                pickedDateTime.set(year, month, day, hour, minute)
                saveYear = year
                saveMonth = month
                saveDay = day
                saveHour = hour
                saveMinute = minute
                currentDateTime.set(saveYear, saveMonth, saveDay, saveHour, saveMinute)
                val time = currentDateTime.timeInMillis
                val timeHour = "$saveYear-${saveMonth + 1}-$saveDay $saveHour:$saveMinute"
                movie.dateHourReminder = timeHour
                movie.reminderTime = time.toString()
                if (databaseOpenHelper.checkReminderExist(movie.id) > 0) {
                    if (databaseOpenHelper.updateReminder(movie) > 0) {
                        NotificationUtil().cancelNotification(movie.id, requireContext())
                        Toast.makeText(context, "updated", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    if (databaseOpenHelper.addReminder(movie) > 0) {
                        Toast.makeText(context, "added", Toast.LENGTH_SHORT).show()
                    }
                }
                detailCallBack.onLoadReminder()
                createNotification(time)
            }, startHour, startMinute, true).show()
        }, startYear, startMonth, startDay).show()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotification(time: Long) {
        val intent = Intent(requireContext().applicationContext, AlarmReceiver::class.java)
        val title = movie.title
        val message = "Release: ${movie.releaseDate} Rate: ${movie.voteAverage}/10"
        intent.putExtra("titleExtra", title)
        intent.putExtra("messageExtra", message)
        val bundle = Bundle()
        bundle.putInt("movieId", movie.id)
        bundle.putString("time", movie.dateHourReminder)
        intent.putExtras(bundle)
        val pendingIntent = PendingIntent.getBroadcast(
            requireActivity().applicationContext,
            notificationID,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        val alarmManager = requireActivity().getSystemService(ALARM_SERVICE) as AlarmManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, pendingIntent)
        }
    }
}