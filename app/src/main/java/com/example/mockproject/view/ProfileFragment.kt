package com.example.mockproject.view

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.mockproject.R
import com.example.mockproject.callback.ProfileCallBack
import com.example.mockproject.util.ImageConverter
import com.google.android.material.radiobutton.MaterialRadioButton
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*


class ProfileFragment : Fragment() {
    private lateinit var name: EditText
    private lateinit var mail: EditText
    private lateinit var dob: TextView
    private lateinit var radioGroup: RadioGroup
    private lateinit var male: MaterialRadioButton
    private lateinit var female: MaterialRadioButton
    private lateinit var profilePic: CircleImageView
    private lateinit var done: Button
    private lateinit var cancel: Button

    private var bitMapProfile: Bitmap? = null
    private var imgConverter: ImageConverter = ImageConverter()
    private var gender: String = ""

    private var saveDay = 0
    private var saveMonth = 0
    private var saveYear = 0

    private val REQUEST_IMAGE_CAPTURE = 1

    lateinit var profileCallBack: ProfileCallBack

    fun setProfilCallBack(callBack: ProfileCallBack) {
        profileCallBack = callBack
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_profile, container, false)
        name = v.findViewById(R.id.edit_name)
        mail = v.findViewById(R.id.edit_mail)
        dob = v.findViewById(R.id.dob_pro)
        radioGroup = v.findViewById(R.id.radio_group)
        male = v.findViewById(R.id.male)
        female = v.findViewById(R.id.female)
        onClickChanged()
        profilePic = v.findViewById(R.id.pro_pic)
        done = v.findViewById(R.id.done)
        cancel = v.findViewById(R.id.cancel)

        val bundle = arguments
        if (bundle != null) {
            var nameBundle = bundle.getString("name")
            var mailBundle = bundle.getString("email")
            var dobBundle = bundle.getString("dob")
            if (nameBundle == "TextView") nameBundle = ""
            if (mailBundle == "TextView") mailBundle = ""
            if (dobBundle == "TextView") dobBundle = "1990-1-1"

            try {
                profilePic.setImageBitmap(imgConverter.StringToBitMap(bundle.getString("imgBitMapString")))
            } catch (e: Exception) {
                profilePic.setImageResource(R.drawable.ic_baseline_face_24)
            }

            name.setText(nameBundle, TextView.BufferType.EDITABLE)
            mail.setText(mailBundle, TextView.BufferType.EDITABLE)
            dob.text = dobBundle

        }

        profilePic.setOnClickListener {
            dispatchTakePictureIntent()
        }

        dob.setOnClickListener {
            setDate()
        }

        done.setOnClickListener {
            val ten = name.text.toString()
            val thu = mail.text.toString()
            val ns = dob.text.toString()
            if (ten == "" || thu == "" || ns == "" || gender == "") {
                Toast.makeText(context, "fill all information", Toast.LENGTH_SHORT).show()
            } else {
                profileCallBack.onSaveProfile(ten, thu, ns, gender, bitMapProfile)
                activity?.supportFragmentManager?.popBackStack()
            }

        }

        cancel.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
        return v
    }

    private fun onClickChanged() {
        radioGroup.setOnCheckedChangeListener { _, i ->
            if (i == R.id.male) {
                gender = "Male"
            } else if (i == R.id.female) {
                gender = "Female"
            }
        }
    }

    private fun setDate() {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(requireContext(), { _, year, month, day ->
            val pickedDateTime = Calendar.getInstance()
            pickedDateTime.set(year, month, day)
            saveYear = year
            saveMonth = month
            saveDay = day
            currentDateTime.set(saveYear, saveMonth, saveDay)
            dob.text = "$saveYear-${saveMonth + 1}-$saveDay"
        }, startYear, startMonth, startDay).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data!!.extras!!.get("data") as Bitmap
            this.bitMapProfile = imageBitmap
            profilePic.setImageBitmap(imageBitmap)
        }
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {

        }
    }
}