package com.example.mockproject.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.mockproject.R
import com.example.mockproject.adapter.FavoriteAdapter
import com.example.mockproject.callback.FavoriteCallBack
import com.example.mockproject.database.DatabaseOpenHelper
import com.example.mockproject.model.Movie


class FavoriteFragment(private val databaseOpenHelper: DatabaseOpenHelper) : Fragment(),
    View.OnClickListener {
    private var moviesDb = ArrayList<Movie>()
    private lateinit var adapter: FavoriteAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var favoriteCallBack: FavoriteCallBack

    fun setFavoriteCallBack(CallBack: FavoriteCallBack) {
        favoriteCallBack = CallBack
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_favorite, container, false)
        moviesDb = databaseOpenHelper.getListMovie()
        adapter = FavoriteAdapter(moviesDb, this)
        recyclerView = v.findViewById(R.id.favorites)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        swipeRefreshLayout = v.findViewById(R.id.fav_refresh)
        val handler = Handler(Looper.getMainLooper())
        swipeRefreshLayout.setOnRefreshListener {
            moviesDb.clear()
            moviesDb = databaseOpenHelper.getListMovie()
            adapter.updateList(moviesDb)
            handler.postDelayed({
                swipeRefreshLayout.isRefreshing = false
            }, 1500)
        }
        return v
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.favorite_button -> {
                val pos = view.tag as Int
                val movieItem = moviesDb[pos]
                if (movieItem.isFavorite) {
                    if (databaseOpenHelper.deleteMovie(movieItem.id) > -1) {
                        movieItem.isFavorite = false
                        adapter.notifyItemChanged(pos)
                        favoriteCallBack.updateBadge(false)
                    } else {
                        Log.e("eeeeeeeeeeeeeeeeeee", "Remove failed")
                    }
                } else {
                    if (databaseOpenHelper.addMovie(movieItem) > -1) {
                        movieItem.isFavorite = true
                        adapter.notifyItemChanged(pos)
                        favoriteCallBack.updateBadge(false)
                    } else {
                        Log.e("eeeeeeeeeeeeeeeeeee", "add failed")
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        requireActivity().title = "Favorites"
        moviesDb.clear()
        moviesDb = databaseOpenHelper.getListMovie()
        adapter.updateList(moviesDb)
    }
}