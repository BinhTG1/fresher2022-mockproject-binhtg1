package com.example.mockproject.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.example.mockproject.R

class AboutFragment : Fragment() {

    lateinit var web:WebView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_about, container, false)
        web = v.findViewById(R.id.webView)
        web.loadUrl("https://www.themoviedb.org/?language=vi")
        return v
    }

    override fun onResume() {
        requireActivity().title = "About"
        super.onResume()
    }
}