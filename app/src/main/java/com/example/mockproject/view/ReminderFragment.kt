package com.example.mockproject.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mockproject.R
import com.example.mockproject.adapter.ReminderAdapter
import com.example.mockproject.callback.ReminderCallBack
import com.example.mockproject.database.DatabaseOpenHelper
import com.example.mockproject.model.Movie
import com.example.mockproject.util.NotificationUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder


class ReminderFragment(private val databaseOpenHelper: DatabaseOpenHelper) : Fragment(),
    View.OnClickListener, View.OnLongClickListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ReminderAdapter
    private lateinit var movieList: ArrayList<Movie>
    private var pos: Int = 0
    private lateinit var reminderCallBack: ReminderCallBack


    fun setReminderCallBack(CallBack: ReminderCallBack) {
        reminderCallBack = CallBack
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_reminder, container, false)
        requireActivity().title = "Reminders"
        movieList = databaseOpenHelper.getListReminder()
        recyclerView = v.findViewById(R.id.reminder_all)
        adapter = ReminderAdapter(0, this, this, movieList)
        adapter.settingMovieFavorite(databaseOpenHelper.getListMovie())
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)

        return v
    }

    override fun onClick(view: View) {
        pos = view.tag as Int
        reminderCallBack.onClickItemReminder(movieList[pos])
    }

    override fun onLongClick(view: View): Boolean {
        pos = view.tag as Int
        showALertDialog(pos)
        return false
    }
    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.getItem(4)
        item.isVisible = false
        super.onPrepareOptionsMenu(menu)
    }

    fun showALertDialog(position: Int) {
        MaterialAlertDialogBuilder(requireContext()).setTitle("Confirm Action")
            .setMessage("Do you want to cancel/delete this reminder?")
            .setNegativeButton("Cancel") { _, _ ->


            }.setPositiveButton("Delete") { _, _ ->
            if (databaseOpenHelper.deleteReminder(movieList[position].id) > -1) {
                Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show()
                NotificationUtil().cancelNotification(movieList[position].id, requireContext())
                Toast.makeText(context, "Canceled", Toast.LENGTH_SHORT).show()
                adapter.deleteItem(position)
                reminderCallBack.onLoadReminder()
                if (movieList.size <= 0) {
                    requireActivity().supportFragmentManager.beginTransaction().remove(this)
                        .commit()
                }
            }

        }.show()
    }

}