package com.example.mockproject.view

import android.content.SharedPreferences
import android.os.Bundle
import androidx.preference.*
import com.example.mockproject.R
import com.example.mockproject.callback.SettingCallBack

class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    lateinit var category: ListPreference
    lateinit var rate: SeekBarPreference
    lateinit var date: EditTextPreference
    lateinit var sort: ListPreference
    lateinit var settingCallBack: SettingCallBack

    fun setsettingCallBack(callBack: SettingCallBack) {
        settingCallBack = callBack
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        category = findPreference("movie_category")!!
        rate = findPreference("movie_rated")!!
        date = findPreference("movie_release_year")!!
        sort = findPreference("movie_sort")!!

        val sharedPreferences = activity?.let { PreferenceManager.getDefaultSharedPreferences(it) }
        if (sharedPreferences != null) {
            category.summary = when(sharedPreferences.getString("movie_category", "Popular")) {
                "top_rated" -> "Top Rated"
                "upcoming" -> "Upcoming"
                "now_playing" -> "Now Playing"
                else -> "Popular"
            }


            rate.summary = sharedPreferences.getInt("movie_rated", 0).toString()

            date.summary = sharedPreferences.getString("movie_release_year", "None")

            sort.summary = when(sharedPreferences.getString("movie_sort", "None")) {
                "none" -> "None"
                "release" -> "Release"
                "rate" -> "Rating"
                else -> "None"
            }

        }




    }
     override fun onResume() {
         requireActivity().title = "Settings"
        super.onResume()
        // Set up a listener whenever a key changes
         preferenceScreen.sharedPreferences
             ?.registerOnSharedPreferenceChangeListener(this)
    }

     override fun onPause() {
        super.onPause()
        // Unregister the listener whenever a key changes
         preferenceScreen.sharedPreferences
             ?.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String?) {
        val cat = sharedPreferences.getString("movie_category", "popular")
        val rat = sharedPreferences.getInt("movie_rated", 0)
        val ate = sharedPreferences.getString("movie_release_year", "")
        val sor = sharedPreferences.getString("movie_sort", "none")

        when{
            key.equals("movie_category")-> {
                category.summary = when(cat) {
                    "top_rated" -> "Top Rated"
                    "upcoming" -> "Upcoming"
                    "now_playing" -> "Now Playing"
                    else -> "Popular"
                }
            }
            key.equals("movie_rated") -> {
                if(rat!=0) {
                    rate.summary = rat.toString()
                }

            }

            key.equals("movie_release_year") -> {
                if(!ate.isNullOrEmpty()) {
                    date.summary = ate
                }
            }

            key.equals("movie_sort") -> {
                sort.summary = when(sor) {
                    "none" -> "None"
                    "release" -> "Release"
                    "rate" -> "Rating"
                    else -> "None"
                }
            }
        }

        settingCallBack.onUpdateFromSetting(cat!!, rat.toString(), ate!!, sor.toString())

    }
}