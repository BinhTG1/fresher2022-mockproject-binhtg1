package com.example.mockproject.controller

import com.example.mockproject.adapter.CastAdapter
import com.example.mockproject.adapter.MovieAdapter
import com.example.mockproject.api.ApiInterface
import com.example.mockproject.api.RetrofitClient
import com.example.mockproject.callback.ApiCallBack
import com.example.mockproject.constant.APIConstant
import com.example.mockproject.model.CastCrew
import com.example.mockproject.model.CastCrewList
import com.example.mockproject.model.Movie
import com.example.mockproject.model.MovieList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiData {
    lateinit var apiCallBack: ApiCallBack

    fun setapiCallBack(callBack: ApiCallBack) {
        apiCallBack = callBack
    }

    fun getListMovieFromApi(
        pageCurrent: Int,
        listMovieCurrent: ArrayList<Movie>,
        adapter: MovieAdapter,
        listMovieDb: ArrayList<Movie>,
        typeListMovie: String,
        rate: Double,
        releaseYear: String,
        sortBy: String,
        isLoadMore: Boolean,
        isRefresh:Boolean
    ): Int {

        var page = pageCurrent
        if (isLoadMore) page++
        else if (!isRefresh) {
            apiCallBack.loadProgress(false)
        }

        val retrofit: ApiInterface =
            RetrofitClient().getRetrofitInstance().create(ApiInterface::class.java)
        val retrofitData = retrofit.getMovieList(typeListMovie, APIConstant.API_KEY, "$page")
        retrofitData.enqueue(object : Callback<MovieList> {
            override fun onResponse(call: Call<MovieList>?, response: Response<MovieList>?) {
                val responseBody = response?.body()
                var listMovieResult = responseBody?.results as ArrayList<Movie>
                listMovieResult = setUpMovieSettingAPI(listMovieResult, rate, releaseYear, sortBy)
                if(isRefresh) {
                    listMovieCurrent.clear()
                }
                listMovieResult.forEach {
                    if (it !in listMovieCurrent) {
                        listMovieCurrent.add(it)
                    }
                }
                apiCallBack.loadProgress(true)
                adapter.updateList(listMovieCurrent)
                adapter.settingMovieFavorite(listMovieDb)
            }

            override fun onFailure(call: Call<MovieList?>, t: Throwable) {
                if (isLoadMore) page--
                else apiCallBack.loadProgress(true)
            }

        }
        )
        if(isLoadMore) {
            return page
        } else {
            return 1
        }
    }

    fun getCastCrewFromApi(
        castCrewList: ArrayList<CastCrew>,
        adapter: CastAdapter,
        movieId: Int
    ) {
        val retrofit: ApiInterface =
            RetrofitClient().getRetrofitInstance().create(ApiInterface::class.java)
        val retrofitData = retrofit.getCastandCrew(movieId, APIConstant.API_KEY)
        castCrewList.clear()
        retrofitData.enqueue(object : Callback<CastCrewList?> {
            override fun onResponse(call: Call<CastCrewList?>, response: Response<CastCrewList?>) {
                val responseBody = response.body()
                castCrewList.addAll(responseBody!!.castList)
                castCrewList.addAll(responseBody.crewList)
                adapter.updateList(castCrewList)
            }

            override fun onFailure(call: Call<CastCrewList?>, t: Throwable) {

            }
        })

    }

    fun setUpMovieSettingAPI(movieList:ArrayList<Movie>, rate:Double, releaseYear:String, sortBy: String) : ArrayList<Movie> {

        movieList.removeAll{it.voteAverage < rate}

        val convertedYear: Int? = if(releaseYear.length>3) {
            releaseYear.substring(0, 4).trim().toIntOrNull()
        } else {
            null
        }

        if(convertedYear!=null) {
            movieList.removeAll{
                it.releaseDate.substring(0,4).trim().toInt() < convertedYear
            }
        }
        if(sortBy == "release") {
            movieList.sortByDescending { it.releaseDate }
        } else if(sortBy == "rate")  {
            movieList.sortByDescending { it.voteAverage }
        }
        return movieList
    }
}