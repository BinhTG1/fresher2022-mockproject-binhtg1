package com.example.mockproject.callback

interface DetailCallBack {
    fun updateBadge(isAdded: Boolean)
    fun updateFavo(movieId: Int, isFavo: Boolean)
    fun onLoadReminder()
}