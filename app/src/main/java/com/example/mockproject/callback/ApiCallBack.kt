package com.example.mockproject.callback

interface ApiCallBack {
    fun loadProgress(isDone:Boolean)
}