package com.example.mockproject.callback

interface SettingCallBack {
    fun onUpdateFromSetting(category:String, rate:String, releaseYear:String, sort:String)
}