package com.example.mockproject.callback

import android.graphics.Bitmap

interface ProfileCallBack {
    fun onSaveProfile(
        name: String,
        mail: String,
        dob: String,
        gender: String,
        bitmapProfile: Bitmap?
    )
}