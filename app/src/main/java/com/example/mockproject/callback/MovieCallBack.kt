package com.example.mockproject.callback

interface MovieCallBack {
    fun updateBadge(isAdded: Boolean)
    fun onLoadReminder()
}