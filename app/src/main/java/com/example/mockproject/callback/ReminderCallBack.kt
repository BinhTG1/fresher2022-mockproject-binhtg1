package com.example.mockproject.callback

import com.example.mockproject.model.Movie

interface ReminderCallBack {
    fun onLoadReminder()
    fun onClickItemReminder(movie: Movie)
}