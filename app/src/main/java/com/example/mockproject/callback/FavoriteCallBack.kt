package com.example.mockproject.callback

interface FavoriteCallBack {
    fun updateBadge(isAdded: Boolean)
}